#ifndef ARM
#define ARM

//This file contains the code to control the robot arm


#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_MotorShield.h"
#include "utility\Adafruit_MS_PWMServoDriver.h"


//Macros used for passing into the moveArm function
#define UP true
#define DOWN false

const int STEPS = 200; //Number of steps per rotation
//const int channel = 1; //Channel on motor shield (either 1 or 2)
//const int RPM = 200;

Adafruit_MotorShield motorShield;

Adafruit_StepperMotor* stepperMotor;

//FUNCTION MUST BE RAN BEFORE RUNNING OTHER FUNCTIONS IN THIS FILE
void initializeArm(const int channel = 1, const int rpm = 100)
{
	motorShield = Adafruit_MotorShield(0x61);
	motorShield.begin();
	stepperMotor = motorShield.getStepper(STEPS, channel);
	stepperMotor->setSpeed(rpm);
}

//Move arm by number of degrees Positive and negative numbers can be used to move motor forwards or backwards
void moveArm(const int steps, const bool direction )
{
	//int steps = 0;
	const int gearRatio = 7;
	if (direction)
	{
		stepperMotor->step(steps*gearRatio, BACKWARD, DOUBLE);
	}
	else {
		stepperMotor->step(steps * gearRatio, FORWARD, DOUBLE);
	}
	stepperMotor->release();
}




//const int PWMA = 3; //SPARKFUN MOTOR CONTROLLER PINS  (OTHER MOTOR CONTROLLERS USE DIFFERENT PINS)
//const int DIRA = 12;
//const int PWMB = 11;
//const int DIRB = 13;
//
//
//
//
////WARNING!!!!!!!!!!!! MOVE ARM BACKARDS THIS FUNCTION DOES NOT STOP THE ARM (THE DIRECTION THAT WILL SMASH THE INSIDE OF THE ROBOT)
//void armBackward()
//{
//	digitalWrite(DIRA, HIGH);
//	analogWrite(PWMA, 255);
//}
//
////STOP THE ARM FROM MOVING
//void armStop()
//{
//	analogWrite(PWMA, 0);
//}
//
////MOVE ARM FORWARDS THIS FUNCTION WILL NEVER STOP THE ARM (THE DIRECTION THAT WON'T SMASH THE INSIDE OF THE ROBOT)
//void armForward()
//{
//	digitalWrite(DIRA, LOW);
//	analogWrite(PWMA, 255);
//}

#endif