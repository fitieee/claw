**Author**: Matthew Ridley

**Description**: The purpose of this library is to provide functions for easy use of the claw and arm.

---


##**Instructions**: There are two methods of using the code

 1. Add the ClawArm folder to your Arduino/libraries folder. 
 2. Open the Arduino IDE and go to Sketch->Include Libraries
 3. Scroll down to Contributed Libraries and select ClawArm
 
###**Necessary Libraries**
 All libraries can be included through the Arduino IDE
 + Servo
 + Wire
 + Adafruit Motor Shield V2
 + Adafruit PWM Servo Driver

---

##**Functions**

###**Arm**

void initializeArm(const int channel=1, const int rpm=100)

    Description:
        Initializes use of the arm stepper motor
        (Must be ran before arm can be used)
    Parameters:
        channel- motor channel on Adafruit motor shield.
        Set to 1 if using M1 and M2 on motor shield or 
        set to 2 if using M3 and M4.
        
        rpm - speed in which the stepper motor moves

void moveArm(const int steps, const bool direction)
    
    Description:
        Move the arm of the robot up or down
    Paramters:
        steps- number of steps the stepper moves.
        200 steps is 360 degrees of rotation on the arm
            Note: 200 steps is not 360 degrees of the stepper rotation.
            
        direction - the direction that arm will move in
        There are defined macros for these
            UP = false (move the arm up)
            DOWN = true (move the arm down)

###**Claw**

void initializeClaw()
    
    Description:
        Initizlizes use of the claw servo motor
        (Must be ran before claw can be used)
    
    
void openClaw()

    Description:
        Opens the claw to a fully open position
        
        
void closeClaw()

    Description:
        Closes the claw enough to carry a 1 inch block
        

        
    
---        

##**Change Log**
3/18/16: 
+ Added functions to control the claw

3/20/16: 
+ Added basic arm control (forward, backward, stop) functinos
+ Added outlines for arm calibration function, and block pickup function

3/26/16: 
+ Changed Arm code to work with stepper motor instead of DC Motor.
+ Replaced forwad, backward, and stop functions with a single moveArm function.

3/27/16: 
+ Removed Arduino libraries from folder. 
+ Fixed Claw code to work with Adafruit motor shield
+ Changed arm code to work in steps instead of degrees
+ Added scale factor to steps to scale with arm gear size
         
