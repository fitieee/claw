#include <clawArm.h>

void setup() {
  // put your setup code here, to run once:

initializeStepper();
initializeClaw();

moveArm(50, UP); //Move arm up 50 steps (1/4 full rotation)

openClaw(); //Open Claw

closeClaw(); //Close claw to grab block

moveArm(50, DOWN); //Move arm back down

}

void loop() {
  // put your main code here, to run repeatedly:
}
