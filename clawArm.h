#ifndef CLAWARM
#define CLAWARM

#include "Arduino.h"
#include "Arm.h"
#include "Servo.h"

const int clawOpen = 150;
const int clawClosed = 65;

const int servoPin = 10;

Servo clawServo;

//Initialize pin to send signals to motor (RUN BEFORE USING CLAW)
void initializeClaw()
{
	clawServo.attach(servoPin);
}

//Open claw all the way
void openClaw() {
	clawServo.write(clawOpen);
}

//Close the claw to pick up a block
void closeClaw() {
	clawServo.write(clawClosed);
}




//Placeholder Functions
void calibrateArm()
{

}

void pickupBlock()
{

}



#endif